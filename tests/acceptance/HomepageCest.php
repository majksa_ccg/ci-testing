<?php

use Codeception\Example;

class HomepageCest
{
    /**
     * Undocumented function
     * @param AcceptanceTester $I
     * @return void
     */
    public function tryToTest(AcceptanceTester $I): void
    {
        $I->wantTo('test homepage');
        $I->amOnPage("/?username=ondra");
        $I->see('Welcome ondra');
    }
}
