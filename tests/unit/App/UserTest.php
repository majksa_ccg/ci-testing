<?php namespace App;

use Codeception\Example;
use Codeception\Test\Unit;
use UnitTester;

class UserTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    /**
     * @dataProvider usernameProvider
     */
    public function testValidate(string $username, bool $result)
    {
        $user = new User();
        $user->username = $username;
        $this->tester->assertEquals($result, $user->validate());
    }

    public function usernameProvider()
    {
        return [
            ["username" => "tooooloooongggggnaaaaammeeee", "result" => false],
            ["username" => "0123456789majksaa", "result" => false],
            ["username" => "majksa", "result" => true],
            ["username" => "0123456789majksa", "result" => true],
        ];
    }
}
