<?php declare(strict_types = 1);

namespace App;

class User
{
    public string $username;

    public function validate(): bool
    {
        return strlen($this->username) <= 16;
    }
}