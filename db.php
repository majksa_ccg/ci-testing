<?php

declare(strict_types = 1);

try {
    $pdo = new PDO("mysql:host=localhost;dbname=database", 'user', 'password');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Connection failed: ' . $e->getMessage());
}

$pdo->query("CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),h

    City varchar(255)
);");

$pdo->prepare('INSERT INTO Persons (PersonID, LastName, FirstName, Address, City) VALUES (:PersonID, :LastName, :FirstName, :Address, :City);')->execute([
	':PersonID' => 1,
	':LastName' => 'John',
	':FirstName' => 'Smith',
	':Address' => '123 Main St',
	':City' => 'San Francisco'
]);

$query = $pdo->query('SELECT * FROM Persons;');

var_dump($query->fetchAll());
